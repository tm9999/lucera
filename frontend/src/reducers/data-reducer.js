const dataReducer = (state=[], action) => {
	console.log('datareducer', state, action)
  switch (action.type) {
	  case 'FETCH_DATA_BEGIN' : {
	  	return {...state, loading: true, error: null}
	  }
	  case 'FETCH_DATA_SUCCESS' : {
	  	return {...state, items: action.items, loading: false}
	  }
	  case 'FETCH_DATA_ERROR' : {
	  	return {...state, error: action.error, loading: false}
	  }
	  default: {
	    return state
		}
  }
}

export default dataReducer