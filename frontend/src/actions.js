import { combineEpics } from 'redux-observable'
import { filter, map, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'
import { loadData } from './data-service.js'


const dataSource = [
  {
    key: '1',
    name: 'Mike',
    age: 32,
    address: '10 Downing Street',
  },
  {
    key: '2',
    name: 'John',
    age: 42,
    address: '10 Downing Street',
  },
];
const fetchData = (action$, state$) => action$.pipe(
 filter(action => action.type ==='FETCH_DATA_BEGIN'),
 switchMap(async action => {
 	console.log('fetching data')
 	const items =  await loadData()
 	console.log('loaded data', items)
 	return ({type: 'FETCH_DATA_SUCCESS', items: items})
 })
)

const rootEpic = combineEpics(fetchData)

export default rootEpic