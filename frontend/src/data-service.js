const url = 'http://localhost:3035/data'

const loadData = async () => {
	let data = await fetch(url).then(res => res.json())
	return data
}

export { loadData }