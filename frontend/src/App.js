import React from 'react';
import { Route, NavLink, Redirect } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'
import { history } from './store.js'
import './App.css';
import { Button } from 'antd';
import 'antd/dist/antd.css';
import TableView from './components/table-view.js'
import ChartView from './components/chart-view.js'


function App() {
  return (
    <ConnectedRouter history={history}>
      <div className="App">
        <header className="App-header">
          <span> Lucera </span>
        </header>
        <div className="switch-container">
          <NavLink to="/table"  activeStyle={{ border: '1px solid #1890ff', color: '#1890ff' }}>TABLE </NavLink>
          <NavLink to="/chart"  activeStyle={{ border: '1px solid #1890ff',color: '#1890ff' }}>CHART </NavLink> 
        </div>
          <Route path="/table" component={TableView} />
          <Route path="/chart" component={ChartView} />
          <Redirect from="/" to="table" />
      </div>
    </ConnectedRouter>
  );
}

export default App;
