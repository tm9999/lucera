import React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { Table } from 'antd'



const columns = [
  {title: 'Sym', dataIndex: 'sym', key: 'id' },
  {title: 'Timestamp', dataIndex: 'ts', render: p => `${moment(p).format('MM/DD HH:mm:ss.SSS')}`},
  {title: 'LP', dataIndex: 'lp',
	  filters: [...Array(10).keys()].map(i => ({text: 'LP'+i, value: 'LP'+i})),
	  onFilter: (value, record) => record.lp.indexOf(value) === 0,
	  filterMultiple: false
	},
  {title: 'Bid Price', dataIndex: 'bid_price', render: p => `$${p.toLocaleString()}`, sorter: (a, b) => a.bid_price - b.bid_price},
  {title: 'Bid Quantity', dataIndex: 'bid_quantity', render: p => `${p.toLocaleString()}`,sorter: (a, b) => a.bid_quantity - b.bid_quantity},
  {title: 'Ask Price', dataIndex: 'ask_price', render: p => `$${p.toLocaleString()}`,sorter: (a, b) => a.ask_price - b.ask_price},
  {title: 'Ask Quantity', dataIndex: 'ask_quantity', render: p => `${p.toLocaleString()}`,sorter: (a, b) => a.ask_quantity - b.ask_quantity},
];

const TableViewPresentation = (props) => {
	console.log('tableview',props)
	// props.fetchData()
	return (
		<Table dataSource={props.items} columns={columns} key={i=>i.id} />
	)
}

const TableView = connect(
	state => ({items: state.data.items}),
	dispatch => ({
			fetchData: () => {dispatch({type: 'FETCH_DATA_BEGIN'})}
		})

	)(TableViewPresentation)

export default TableView;
