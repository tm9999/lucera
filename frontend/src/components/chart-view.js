import React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'

import { ResponsiveContainer, ScatterChart, Scatter, LineChart, Line, XAxis, YAxis, ReferenceLine,
  ReferenceDot, Tooltip, CartesianGrid, Legend, Brush, ErrorBar, AreaChart, Area,
  Label, LabelList } from 'recharts';

const ChartViewPresentation = (props) => {
	let original = props.items
	let data = original.map(d => {
		d.value = d.bid_price
		d.time = (new Date(d.ts)).getTime()/1000
		return d

	})

	return (
		<div style={{margin:'0 auto'}}>
	    <h3>chart view</h3>
	    <ResponsiveContainer width = '95%' height = {500} >
	        <ScatterChart>
	          <XAxis
	            dataKey = 'time'
	            domain = {['auto', 'auto']}
	            name = 'Time' 
	            tickFormatter = {(unixTime) => moment(unixTime).format('HH:mm:ss.SSS')}
	            type = 'number'
	          />
	          <YAxis domain={['auto', 'auto']} dataKey = 'value' name = 'Value' />

	          <Scatter
	            data = {data}
	            line = {{ stroke: '#eee' }}
	            lineJointType = 'monotoneX'
	            lineType = 'joint'
	            name = 'Values'
	          />

	        </ScatterChart>
	      </ResponsiveContainer>
      </div>
	)
}


const ChartView = connect(
	state => ({items: state.data.items}),

	)(ChartViewPresentation)

export default ChartView;
