import { createStore, applyMiddleware, compose } from 'redux'
import { createEpicMiddleware } from 'redux-observable'
import { createBrowserHistory } from 'history'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import rootEpic from './actions'
import createRootReducer from './reducers/index.js'

var epicMiddleware = createEpicMiddleware()
const history = createBrowserHistory()

const initialState = {
	data:{
		items: [],
		loading: false,
		error:null
	},
}

let store = createStore(
	createRootReducer(history),
	initialState,
	compose(
		applyMiddleware(
		 routerMiddleware(history),
		 epicMiddleware
		)
	)
)


epicMiddleware.run(rootEpic)
store.dispatch({type:'FETCH_DATA_BEGIN'})

export { history, store }