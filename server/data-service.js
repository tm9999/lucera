const knex = require('knex')
const TABLE_NAME = 'adata'

const db = knex({
	client: 'sqlite3',
	connection: {
	  filename: "./data.sqlite",
	}
})

const allData = async () => await db.select('*').from(TABLE_NAME)
const topNData = async (n) => await db.select('*').from(TABLE_NAME).limit(n)



module.exports = { allData, topNData }