
const express                        = require('express');
const cors                           = require('cors');
const hostname                       = 'localhost';
const port                           = 3035;
const { topNData } = require('./data-service.js')

const app = express();
app.use(cors());//cross origin rquests

app.get('/data', async (req, res) => res.json(await topNData(200)));
app.get('/data/search', (req, res) => {
    const search = req.query.search;
    // res.json(productSearch(search));
});

app.listen(port, () => console.log(`[Server running on ${hostname}:${port}]`));


